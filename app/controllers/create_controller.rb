class CreateController < ApplicationController
  
    def index
#        getDatas
        getSaltRole
        getProxmoxPool
        getProxmoxOS      
        @defaultFQDN = Rails.configuration.application['DEFAULT_VM_FQDN']
        @defaultStack = Rails.configuration.application['DEFAULT_STACK']
        @ipv4Enabled = Rails.configuration.application['ENABLE_IPV4']
        @ipv6Enabled = Rails.configuration.application['ENABLE_IPV6']
        @haEnabled = Rails.configuration.application['HA_VM_SUPPORT']
    end


    def getDatas
        NetboxClientRuby.configure do |c|
        c.netbox.auth.token = current_user.netbox_token    
        c.netbox.api_base_url = Rails.configuration.application['NETBOX_API_URL']
        end
        cluster_id = Rails.configuration.application['NETBOX_VM_CLUSTER_ID']
        begin
            cluster_name = NetboxClientRuby.virtualization.cluster(cluster_id)
            @cluster_name = cluster_name.name
        rescue => exception
            flash[:error] = "Impossible de se connecter à Netbox."
            @cluster_name = nil
        end
    end


    # VM Creation
    def post 
        NetboxClientRuby.configure do |c|
            c.netbox.auth.token = current_user.netbox_token    
            c.netbox.api_base_url = Rails.configuration.application['NETBOX_API_URL']
        end
        # Set configuration for Post
        begin
            cluster_name = params[:cluster]
            for cluster in Rails.configuration.application['CLUSTERS']
              clu = cluster[0]
              if clu.to_s == cluster_name
                cluster_id = cluster[1][:NETBOX_VM_CLUSTER_ID]
                ipv4_vlan = cluster[1][:IPV4_VLAN]
                ipv6_vlan = cluster[1][:IPV6_VLAN]
                prefix = cluster[1][:CLUSTER_IPV6_PREFIX]
              end
            end
            role_id = Rails.configuration.application['NETBOX_VM_ROLE_ID']
            tenant_id = Rails.configuration.application['NETBOX_VM_TENANT_ID']
            #cluster_id = Rails.configuration.application['NETBOX_VM_CLUSTER_ID']
            #prefix = Rails.configuration.application['CLUSTER_IPV6_PREFIX']
            mac_prefix = Rails.configuration.application['CLUSTER_MAC_PREFIX']
            interface_name = Rails.configuration.application['DEFAULT_INTERFACE']
            rundeck_url = Rails.configuration.application['RUNDECK_URL']
            rundeck_job_id = Rails.configuration.application['RUNDECK_TERRAFORMGEN_JOBID']
            v6_mask = Rails.configuration.application['CLUSTER_IPV6_MASK']
            cf_node = Rails.configuration.application['NETBOX_NODE_CF']
            cf_pool = Rails.configuration.application['NETBOX_POOL_CF']
            cf_roles = Rails.configuration.application['NETBOX_ROLES_CF']
            @defaultStack = Rails.configuration.application['DEFAULT_STACK']
            @ipv4Enabled = Rails.configuration.application['ENABLE_IPV4']
            @ipv6Enabled = Rails.configuration.application['ENABLE_IPV6']            
            @haEnabled = Rails.configuration.application['HA_VM_SUPPORT']            

        rescue => exception
            flash[:error] = "Unable to fetch configuration variables."
        end


        
        begin
            vms = NetboxClientRuby.virtualization.virtual_machines.filter(cluster_id: cluster_id, name: params[:name])

            if vms.total == 0

                begin
                    # If 0 VM Found, we can create the VM
                    v = NetboxClientRuby::Virtualization::VirtualMachine.new
                    v.name = params[:name]
                    v.role = {id: role_id}

                    if tenant_id != "None"
                      v.tenant = {id: tenant_id}
                    end
                    
                    v.cluster = {id: cluster_id}
                    v.platform = {id: params[:os]}
                    v.vcpus = params[:vcpus]
                    v.memory = params[:ram]
                    v.disk = params[:disk]
                    # Prepare CFs
                    if params[:ha] == "true"
                        ha = true
                    else
                        ha = false 
                    end
                    if @haEnabled == true
                        v.custom_fields = {
                        "terraformable": true,
                        "ha": ha,
                        "#{cf_roles}": params[:saltrole],
                        "#{cf_node}": params[:node],
                        "#{cf_pool}": params[:proxmoxpool]}
                    else
                        v.custom_fields = {
                        "terraformable": true,
                        "#{cf_roles}": params[:saltrole],
                        "#{cf_node}": params[:node],
                        "#{cf_pool}": params[:proxmoxpool]}
                    end

                    new_vm = v.save 

                rescue => exception
                    flash[:error] = "Unable to create the VM on Netbox"
                end

                # Lets create the first interface
                begin
                    # Now we can Generate Mac & IPv6
                    mac_end = (1..3).map{"%0.2X"%rand(256)}.join(":")
                    mac =  mac_prefix + mac_end
                    new_i = NetboxClientRuby::Virtualization::Interface.new
                    new_i.name = interface_name
                    new_i.virtual_machine =  new_vm.id
                    new_i.mac_address =  mac
                    if @defaultStack == 6
                        new_i.description = "VLAN " + ipv6_vlan
                    elsif @defaultStack == 4
                        new_i.description = "VLAN " + ipv4_vlan
                    end
                    new_int = new_i.save                   
                rescue => exception
                    flash[:error] = "Unable to create 1st Interface on Netbox"

                    
                end

                # If we use dual-stack, Let's create a second interface (if asked in form)
                begin
                    if @ipv4Enabled == true && @ipv6Enabled == true
                        if params[:IPv4Interface] == "True" ||  params[:IPv6Interface] == "True"
                            interface_second = Rails.configuration.application['SECONDARY_INTERFACE']
                            mac_end2 = (1..3).map{"%0.2X"%rand(256)}.join(":")
                            mac2 =  mac_prefix + mac_end2
                            new_i = NetboxClientRuby::Virtualization::Interface.new
                            new_i.name = interface_second
                            ## ToDo Fix VLAN in descritption 
                            new_i.description = "VLAN " + ipv4_vlan
                            new_i.virtual_machine =  new_vm.id
                            new_i.mac_address =  mac2
                            sec_int = new_i.save 
                        end
                    end
                rescue => exception
                    flash[:error] = "Unable to create 2nd Interface on Netbox"                        
                end

                # Let's create the IP of the first interface
                if @defaultStack == 6
                    begin
                        ipv6 = generateIPv6(mac, prefix)
                        new_ip6 = NetboxClientRuby::IPAM::IpAddress.new
                        new_ip6.address = ipv6 + v6_mask
                        new_ip6.assigned_object_type = "virtualization.vminterface"
                        new_ip6.assigned_object_id = new_int.id
                        new_ip6.nat_outside = false
                        new_ip6.dns_name = params[:name]
                        new_ipv6 = new_ip6.save
                    rescue => exception
                        flash[:error] = "Unable to generate and create IPv6 on Netbox"                        
                    end
                end
                if @defaultStack == 4
                    begin
                        if params[:IPv4CIDR] != "192.168.42.42/32"
                            new_ip4 = NetboxClientRuby::IPAM::IpAddress.new
                            new_ip4.address = params[:IPv4CIDR]
                            new_ip4.assigned_object_type = "virtualization.vminterface"
                            new_ip4.assigned_object_id = new_int.id
                            new_ip4.nat_outside = false
                            new_ip4.dns_name = params[:name]
                            new_ipv4 = new_ip4.save
                        else
                            flash[:error] = "example 192.168.42.42/32 IPv4 not created" 
                        end
                    rescue => exception
                        flash[:error] = "Unable to create IPv4 on Netbox"                                                
                    end
                end

                # Lets create the Second IP if needed.
                if @ipv4Enabled == true && @ipv6Enabled == true
                    begin
                        if params[:IPv4Interface] == "True"
                            if params[:IPv4CIDR] != "192.168.42.42/32"
                                second_ip4 = NetboxClientRuby::IPAM::IpAddress.new
                                second_ip4.address = params[:IPv4CIDR]
                        	second_ip4.assigned_object_type = "virtualization.vminterface"
                                second_ip4.assigned_object_id = sec_int.id
                                second_ip4.nat_outside = false
                                second_ip4.dns_name = params[:name]
                                second_ipv4 = second_ip4.save
                            else
                                flash[:error] = "example 192.168.42.42/32 IPv4 not created"                                                       
                            end
                        end
                        if params[:IPv6Interface] == "True"
                            #2ndipv6 = generateIPv6(mac, prefix)
                            second_ip6 = NetboxClientRuby::IPAM::IpAddress.new
                            second_ip6.address = params[:IPv6CIDR]
                            second_ip6.assigned_object_type = "virtualization.vminterface"
                            second_ip6.assigned_object_id = sec_int.id
                            second_ip6.nat_outside = false
                            second_ip6.dns_name = params[:name]
                            second_ipv6 = second_ip6.save
                        end
                    rescue => exception
                        flash[:error] = "Unable to generate and create 2nd IP on Netbox"                        
                    end
                end

                begin
                    # Now we can patch VM with primary_ip4 and ip6
                    v = NetboxClientRuby.virtualization.virtual_machine(new_vm.id)
                    if @defaultStack == 4 
                        if params[:IPv6Interface] == "True"
                            v.update(name: new_vm.name, cluster: cluster_id, primary_ip4: new_ipv4.id.to_s,  primary_ip6: second_ipv6.id.to_s)
                        else 
                            v.update(name: new_vm.name, cluster: cluster_id, primary_ip4: new_ipv4.id.to_s)
                        end
                    end
                    if @defaultStack == 6
                        if params[:IPv4Interface] == "True"
                            v.update(name: new_vm.name, cluster: cluster_id, primary_ip6: new_ipv6.id.to_s,  primary_ip4: second_ipv4.id.to_s)
                        else 
                            v.update(name: new_vm.name, cluster: cluster_id, primary_ip6: new_ipv6.id.to_s)
                        end
                    end                                    

                rescue => exception
                    flash[:alert] = "Unable to Patch VM for primary_ip"
                end
                
                begin
                    # Let's trigger our Rundeck jobs
                    require 'rundeck'
                    options = { :endpoint => rundeck_url, :api_token => current_user.rundeck_token }
                    rd = Rundeck::Client.new(options)
                    ssl_verify = {:verify => false}
                    out = rd.execute_job(rundeck_job_id)

                rescue => exception
                    flash[:error] = "Unable to launch post-Rundeck job."
                end     

                # Let's log our actions
                msgText = "VM " + params[:name] + " created with " + params[:vcpus] + " vCPUs and " + params[:ram] + " Mo of RAM."
                @note = Note.new(title: "VM Created", text: msgText, author: current_user.email)                
                @note.save
                flash[:created] = "La VM " + params[:name] + " est terraformée."
                # La VM existe déjà, on ne la créer pas. 

            else
                flash[:error] = "La VM existe déjà !"
            end    


        rescue => exception
            nil
            #flash[:error] = "Impossible de se connecter à Netbox"
        end

        redirect_to :controller => 'welcome', :action => 'index'
    end


    def generateIPv6(mac, prefix)
        require 'ipaddr'
        eui48 = mac
        # eliminate all delimiters, note that only ':' is not enough; the standard is '-', but '.' is also sometimes used
        eui48 = eui48.gsub(/[^0-9a-zA-Z]/, '')
        # parse the EUI-48 as a number
        eui48 = eui48.to_i(16)
        # split the EUI-48 into the OUI and the manufacturer-assigned parts
        oui, assigned = eui48.divmod(1 << 24)
        # append 16 zero bits to the OUI
        left = oui << 16
        # add the standard bit sequence
        left += 0xfffe
        # append 24 zero bits
        left <<= 24
        # now we have an EUI-64
        eui64 = left + assigned
        # flip bit index 57 to create a modified EUI-64
        modified_eui64 = eui64 ^ 1 << 57
        # the prefix for link-local IPv6 addresses is fe80::/10, link-local addresses are in the fe80::/64 network
        prefix = IPAddr.new(prefix)
        # the suffix is based on our modified EUI-64
        suffix = IPAddr.new(modified_eui64, Socket::AF_INET6)
        # the final result is the bitwise logical OR of the prefix and suffix
        link_local_ipv6 = prefix | suffix
        # the IP address library knows how to correctly format an address according to RFC 5952 Section 4
        return link_local_ipv6.to_s
    end
    
    
end
